# PPD  Visualisation
## Installation
* `git clone git@gitlab.com:vincent.grandiere/PPD_Visualisation.git`
* `npm install`
* `npm start`


## Participants
* Amine SANAE
* Marie Linda ODI
* Laura BLANCO
* Vincent GRANDIERE


## Contribution
Pour tout ajout de fonctionnalité, merci de se réferer à [ce document](https://gitlab.com/vincent.grandiere/PPD_Visualisation/wikis/%5Bfrontend%5D-Contribution)


