import * as fromReducer from "./index"

describe("research reducer", () => {
  it("getResearchFilter", () => {
    const getResearchResult = fromReducer.getResearchResult
    const givenState = {
      research: {
        result: ["toto", "tata"]
      }
    }
    const expectedResult = ["toto", "tata"]

    expect(getResearchResult(givenState)).toEqual(expectedResult)
  })

  it("getResearchResult", () => {
    const getResearchFilter = fromReducer.getResearchFilter
    const givenState = {
      research: {
        filter: "toto"
      }
    }
    const expectedFilter = "toto"

    expect(getResearchFilter(givenState)).toEqual(expectedFilter)
  })

  it("getResearchErrorMessage", () => {
    const getResearchErrorMessage = fromReducer.getResearchErrorMessage
    const givenState = {
      research: {
        errorMessage: "message"
      }
    }
    const expectedMessage = "message"

    expect(getResearchErrorMessage(givenState)).toEqual(expectedMessage)
  })

  it("getResearchIsLoading", () => {
    const getResearchIsLoading = fromReducer.getResearchIsLoading
    const givenState = {
      research: {
        isLoading: true
      }
    }
    const expectedMessage = true

    expect(getResearchIsLoading(givenState)).toEqual(expectedMessage)
  })
})
