import { combineReducers } from 'redux'
import {
  START_FETCHING_BOX_PLOT,
  FETCH_BOX_PLOT_SUCCESS,
  FETCH_BOX_PLOT_FAILED,
  DELETE_WORD,
} from "../../actions/box-plot"

const nbDimentions = (state = 300, action) => {
  switch (action.type) {
    case FETCH_BOX_PLOT_SUCCESS:
      return action.nbDimensions

      default:
      return state
  }
}

const list = (state = [], action) => {
  switch (action.type) {
    case FETCH_BOX_PLOT_SUCCESS:
      return action.stats

    case DELETE_WORD:
      return state.filter(box => box.word !== action.word)

    default:
      return state
  }
}

const isLoading = (state = false, action) => {
  switch (action.type) {
    case START_FETCHING_BOX_PLOT:
      return true

    case FETCH_BOX_PLOT_SUCCESS:
    case FETCH_BOX_PLOT_FAILED:
      return false

    default:
      return state
  }
}

const errorMessage = (state = "", action) => {
  switch (action.type) {
    case START_FETCHING_BOX_PLOT:
    case FETCH_BOX_PLOT_SUCCESS:
    return ""

    case FETCH_BOX_PLOT_FAILED:
      return action.errorMessage

    default:
      return state
  }
}


export default combineReducers({
  list,
  isLoading,
  errorMessage,
  nbDimentions
})

export const getList = state => state.list
export const getIsLoading = state => state.isLoading
export const getErrorMessage = state => state.errorMessage
export const getNbDimentions = state => state.nbDimentions
