import { combineReducers } from "redux"

import list, * as fromList from "./list"
import toImport, * as fromToImport from "./toImport"

const models = combineReducers({
  list,
  toImport
})

export default models

export const getModelListElements = state => fromList.getElements(state.list)
export const getModelListIsInit = state => fromList.getIsInit(state.list)
export const getModelListIsLoading = state => fromList.getIsLoading(state.list)
export const getModelListErrorMessage = state =>
  fromList.getErrorMessage(state.list)

export const getModelToImportIsSending = state =>
  fromToImport.getIsSending(state.toImport)
export const getModelToImportErrorMessage = state =>
  fromToImport.getErrorMessage(state.toImport)
