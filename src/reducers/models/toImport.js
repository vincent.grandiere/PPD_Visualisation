import { combineReducers } from "redux"
import {
  START_IMPORTING_MODEL,
  IMPORT_MODEL_SUCCESS,
  IMPORT_MODEL_FAILED,
} from "../../actions/models"

const isSending = (state = false, action) => {
  switch (action.type) {
    case START_IMPORTING_MODEL:
      return true

    case IMPORT_MODEL_SUCCESS:
    case IMPORT_MODEL_FAILED:
      return false

    default:
      return state
  }
}

const errorMessage = (state = "", action) => {
  switch (action.type) {
    case IMPORT_MODEL_SUCCESS:
    case START_IMPORTING_MODEL:
      return ""

    case IMPORT_MODEL_FAILED:
      return action.errorMessage

    default:
      return state
  }
}

const selected = combineReducers({
  isSending,
  errorMessage
})

export default selected

export const getIsSending = state => state.isSending
export const getErrorMessage = state => state.errorMessage
