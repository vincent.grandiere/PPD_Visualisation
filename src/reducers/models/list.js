import { combineReducers } from "redux"
import {
  IMPORT_MODEL_SUCCESS,
  START_FETCHING_MODELS,
  FETCH_MODELS_SUCCESS,
  FETCH_MODELS_FAILED,
  SELECT_MODEL
} from "../../actions/models"

const isInit = (state = false, action) => {
  switch (action.type) {
    case FETCH_MODELS_SUCCESS:
      return true

    default:
      return state
  }
}

const elements = (state = [], action) => {
  switch (action.type) {
    case FETCH_MODELS_SUCCESS:
      return action.models

    case IMPORT_MODEL_SUCCESS:
      return [...state, action.model]

    default:
      return state
  }
}

const isLoading = (state = false, action) => {
  switch (action.type) {
    case START_FETCHING_MODELS:
      return true

    case FETCH_MODELS_SUCCESS:
    case FETCH_MODELS_FAILED:
      return false

    default:
      return state
  }
}

const errorMessage = (state = null, action) => {
  switch (action.type) {
    case FETCH_MODELS_FAILED:
      return action.errorMessage

    case FETCH_MODELS_SUCCESS:
    case START_FETCHING_MODELS:
      return null

    default:
      return state
  }
}

const list = combineReducers({
  elements,
  isLoading,
  isInit,
  errorMessage
})

export default list

export const getElements = state => state.elements
export const getIsInit = state => state.isInit
export const getIsLoading = state => state.isLoading
export const getErrorMessage = state => state.errorMessage
