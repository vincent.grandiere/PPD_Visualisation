import { combineReducers } from "redux"
import { routerReducer } from 'react-router-redux'
import {reducer as toastrReducer} from 'react-redux-toastr'

import research, * as fromResearch from "./research"
import models, * as fromModels from "./models"
import corpus, * as fromCorpus from "./corpus"
import words, * as fromWords from "./words"
import boxPlot, * as fromBoxPlot from './box-plot'

const appReducer = combineReducers({
  words,
  research,
  routerReducer,
  models,
  corpus,
  toastr: toastrReducer,
  boxPlot
})

export default appReducer

export const getSelectedWordIsLoading = state => 
  fromWords.getSelectedIsLoading(state.words)
export const getSelectedWordParameters = state => 
  fromWords.getSelectedParameters(state.words)

export const getBoxPlotNbDimentions = state => fromBoxPlot.getNbDimentions(state.boxPlot)
export const getBoxPlotIsLoading = state => fromBoxPlot.getIsLoading(state.boxPlot)
export const getBoxPlotErrorMessage = state => fromBoxPlot.getErrorMessage(state.boxPlot)
export const getBoxPlotList = state => fromBoxPlot.getList(state.boxPlot)

export const getResearchFilter = state =>
  fromResearch.getResearchFilter(state.research)
export const getResearchResult = state =>
  fromResearch.getResearchResult(state.research)
export const getResearchErrorMessage = state =>
  fromResearch.getResearchErrorMessage(state.research)
export const getResearchIsLoading = state =>
  fromResearch.getResearchIsLoading(state.research)

export const getCorpusListElements = state =>
  fromCorpus.getCorpusListElements(state.corpus)
export const getCorpusListIsInit = state =>
  fromCorpus.getCorpusListIsInit(state.corpus)
export const getCorpusListIsLoading = state =>
  fromCorpus.getCorpusListIsLoading(state.corpus)
export const getCorpusListErrorMessage = state =>
  fromCorpus.getCorpusListErrorMessage(state.corpus)

export const getCorpusToImportIsSending = state =>
  fromCorpus.getCorpusToImportIsSending(state.corpus)
export const getCorpusToImportErrorMessage = state =>
  fromCorpus.getCorpusToImportErrorMessage(state.corpus)

export const getModelListElements = state =>
  fromModels.getModelListElements(state.models)
export const getModelListIsInit = state =>
  fromModels.getModelListIsInit(state.models)
export const getModelListIsLoading = state =>
  fromModels.getModelListIsLoading(state.models)
export const getModelListErrorMessage = state =>
  fromModels.getModelListErrorMessage(state.models)

export const getModelToImportIsSending = state =>
  fromModels.getModelToImportIsSending(state.models)
export const getModelToImportErrorMessage = state =>
  fromModels.getModelToImportErrorMessage(state.models)
