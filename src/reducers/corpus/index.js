import { combineReducers } from "redux"

import list, * as fromList from "./list"
import toImport, * as fromToImport from "./toImport"

const corpus = combineReducers({
  list,
  toImport
})

export default corpus

export const getCorpusListElements = state => fromList.getElements(state.list)
export const getCorpusListIsInit = state => fromList.getIsInit(state.list)
export const getCorpusListIsLoading = state => fromList.getIsLoading(state.list)
export const getCorpusListErrorMessage = state =>
  fromList.getErrorMessage(state.list)

export const getCorpusToImportIsSending = state =>
  fromToImport.getIsSending(state.toImport)
export const getCorpusToImportErrorMessage = state =>
  fromToImport.getErrorMessage(state.toImport)
