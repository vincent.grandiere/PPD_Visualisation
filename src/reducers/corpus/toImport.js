import { combineReducers } from "redux"
import {
  START_IMPORTING_CORPUS,
  IMPORT_CORPUS_SUCCESS,
  IMPORT_CORPUS_FAILED,
} from "../../actions/corpus"

const isSending = (state = false, action) => {
  switch (action.type) {
    case START_IMPORTING_CORPUS:
      return true

    case IMPORT_CORPUS_SUCCESS:
    case IMPORT_CORPUS_FAILED:
      return false

    default:
      return state
  }
}

const errorMessage = (state = "", action) => {
  switch (action.type) {
    case IMPORT_CORPUS_SUCCESS:
    case START_IMPORTING_CORPUS:
      return ""

    case IMPORT_CORPUS_FAILED:
      return action.errorMessage

    default:
      return state
  }
}

const selected = combineReducers({
  isSending,
  errorMessage
})

export default selected

export const getIsSending = state => state.isSending
export const getErrorMessage = state => state.errorMessage
