import { combineReducers } from "redux"
import {
  IMPORT_CORPUS_SUCCESS,
  START_FETCHING_CORPUS,
  FETCH_CORPUS_SUCCESS,
  FETCH_CORPUS_FAILED,
} from "../../actions/corpus"

const isInit = (state = false, action) => {
  switch (action.type) {
    case FETCH_CORPUS_SUCCESS:
      return true

    default:
      return state
  }
}

const elements = (state = [], action) => {
  switch (action.type) {
    case FETCH_CORPUS_SUCCESS:
      return action.corpus

    case IMPORT_CORPUS_SUCCESS:
      return [...state, action.corpus]

    default:
      return state
  }
}

const isLoading = (state = false, action) => {
  switch (action.type) {
    case START_FETCHING_CORPUS:
      return true

    case FETCH_CORPUS_SUCCESS:
    case FETCH_CORPUS_FAILED:
      return false

    default:
      return state
  }
}

const errorMessage = (state = null, action) => {
  switch (action.type) {
    case FETCH_CORPUS_FAILED:
      return action.errorMessage

    case FETCH_CORPUS_SUCCESS:
    case START_FETCHING_CORPUS:
      return null

    default:
      return state
  }
}

const list = combineReducers({
  elements,
  isLoading,
  isInit,
  errorMessage
})

export default list

export const getElements = state => state.elements
export const getIsInit = state => state.isInit
export const getIsLoading = state => state.isLoading
export const getErrorMessage = state => state.errorMessage
