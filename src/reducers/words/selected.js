import { combineReducers } from "redux"
import {
  START_FETCHING_ONE_WORD,
  FETCH_ONE_WORD_SUCCESS,
  FETCH_ONE_WORD_FAILED
} from '../../actions/research'

const isLoading = (state = false, action) => {
  switch (action.type) {
    case START_FETCHING_ONE_WORD:
      return true

    case FETCH_ONE_WORD_SUCCESS:
    case FETCH_ONE_WORD_FAILED:
      return false

    default:
      return state
  }
}

const parameters = (state = {}, action) => {
  switch (action.type) {
    case FETCH_ONE_WORD_SUCCESS:
      return action.parameters

    default:
      return state
  }
}

const word = (state = "", action) => {
  switch (action.type) {
    case FETCH_ONE_WORD_SUCCESS:
      return action.word

    default:
      return state
  }
}

export default combineReducers({
  isLoading,
  parameters
})

export const getIsLoading = (state) => state.isLoading
export const getParameters = (state) => state.parameters