import { combineReducers } from "redux"
import selected, * as fromSelected from './selected'

export default combineReducers({
  selected
})

export const getSelectedIsLoading = (state) => fromSelected.getIsLoading(state.selected)
export const getSelectedParameters = (state) => fromSelected.getParameters(state.selected)