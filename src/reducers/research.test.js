import { getResearchFilter } from "./research"

describe("get research filter", () => {
  it("find filter", () => {
    const givenState = {
      filter: "toto"
    }

    const expectedFilter = "toto"

    expect(getResearchFilter(givenState)).toEqual(expectedFilter)
  })
})
