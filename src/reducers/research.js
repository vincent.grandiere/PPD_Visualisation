import { combineReducers } from "redux"

import {
  START_FETCHING_WORDS,
  FETCH_WORDS_SUCCESS,
  FETCH_WORDS_FAILED,
  UPDATE_RESEARCH_FILTER
} from "../actions/research"

const filter = (state = "", action) => {
  switch (action.type) {
    case UPDATE_RESEARCH_FILTER:
      return action.filter

    default:
      return state
  }
}

const result = (state = [], action) => {
  switch (action.type) {
    case FETCH_WORDS_SUCCESS:
      return action.words

    default:
      return state
  }
}

const errorMessage = (state = null, action) => {
  switch (action.type) {
    case FETCH_WORDS_SUCCESS:
    case START_FETCHING_WORDS:
      return null

    case FETCH_WORDS_FAILED:
      return action.errorMessage

    default:
      return state
  }
}

const isLoading = (state = false, action) => {
  switch (action.type) {
    case START_FETCHING_WORDS:
      return true

    case FETCH_WORDS_FAILED:
    case FETCH_WORDS_SUCCESS:
      return false

    default:
      return state
  }
}

const research = combineReducers({
  filter,
  result,
  errorMessage,
  isLoading
})

export default research

export const getResearchFilter = state => state.filter
export const getResearchResult = state => state.result
export const getResearchErrorMessage = state => state.errorMessage
export const getResearchIsLoading = state => state.isLoading
