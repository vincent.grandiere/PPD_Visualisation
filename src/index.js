import React from "react"
import ReactDOM from "react-dom"

import Root from "./components/root"

import configueStore from "./configueStore"
import "./index.css"

const store = configueStore()

ReactDOM.render(<Root store={store} />, document.getElementById("root"))
