import api from "../api"

import { getCorpusListIsLoading, getCorpusToImportIsSending } from "../reducers"
import { push } from "react-router-redux"

export const START_FETCHING_CORPUS = "START_FETCHING_CORPUS"
export const FETCH_CORPUS_SUCCESS = "FETCH_CORPUS_SUCCESS"
export const FETCH_CORPUS_FAILED = "FETCH_CORPUS_FAILED"

export const START_IMPORTING_CORPUS = "START_IMPORTING_CORPUS"
export const IMPORT_CORPUS_SUCCESS = "IMPORT_CORPUS_SUCCESS"
export const IMPORT_CORPUS_FAILED = "IMPORT_CORPUS_FAILED"

export const fetchCorpus = () => (dispatch, getState) => {
  if (getCorpusListIsLoading(getState())) {
    return Promise.resolve()
  }

  dispatch({
    type: START_FETCHING_CORPUS
  })
  return api.fetchCorpus().then(
    corpus => {
      dispatch({
        type: FETCH_CORPUS_SUCCESS,
        corpus
      })
    },
    error => {
      dispatch({
        type: FETCH_CORPUS_FAILED,
        errorMessage: error.message
      })
    }
  )
}

export const importCorpus = file => (dispatch, getState) => {
  if (getCorpusToImportIsSending(getState())) {
    return Promise.resolve()
  }

  dispatch({
    type: START_IMPORTING_CORPUS
  })

  return api.importCorpus(file).then(
    result => {
      dispatch({
        type: IMPORT_CORPUS_SUCCESS,
        corpus: result
      })
    },
    error => {
      dispatch({
        type: IMPORT_CORPUS_FAILED,
        errorMessage: error.message
      })
    }
  )
}

export const selectCorpus = (id) => (dispatch, getState) =>
  dispatch(push(`${window.location.pathname}/corpus/${id}`))
