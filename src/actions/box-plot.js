import api from "../api"

import { getBoxPlotIsLoading } from "../reducers"

export const START_FETCHING_BOX_PLOT = "START_FETCHING_BOX_PLOT"
export const FETCH_BOX_PLOT_SUCCESS = "FETCH_BOX_PLOT_SUCCESS"
export const FETCH_BOX_PLOT_FAILED = "FETCH_BOX_PLOT_FAILED"

export const DELETE_WORD = "DELETE_WORD"

export const getBoxPlot = (idModel, idCorpus) => (dispatch, getState) => {
  if (getBoxPlotIsLoading(getState())) {
    return
  }

  dispatch({ type: START_FETCHING_BOX_PLOT })
  api.fetchBoxPlot(idModel, idCorpus).then(
    result =>
      dispatch({
        type: FETCH_BOX_PLOT_SUCCESS,
        stats: result.stats,
        nbDimensions: result.nbDimensions
      }),
    error =>
      dispatch({
        type: FETCH_BOX_PLOT_FAILED,
        errorMessage: error.message
      })
  )
}

export const deleteWord = word => ({
  type: DELETE_WORD,
  word
})
