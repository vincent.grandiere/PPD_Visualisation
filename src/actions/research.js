import config from "../config"
import {
  getResearchFilter,
  getResearchIsLoading,
  getSelectedWordIsLoading
} from "../reducers"
import { push } from "react-router-redux"
import { toastr } from "react-redux-toastr"

import api from "../api/index"

const MAX_RESULT_PER_REQUEST = config.research.MAX_RESULT_PER_REQUEST

export const UPDATE_RESEARCH_FILTER = "UPDATE_RESEARCH_FILTER"
export const START_FETCHING_WORDS = "START_FETCHING_WORDS"
export const FETCH_WORDS_SUCCESS = "FETCH_WORDS_SUCCESS"
export const FETCH_WORDS_FAILED = "FETCH_WORDS_FAILED"

export const START_FETCHING_ONE_WORD = "START_FETCHING_ONE_WORD"
export const FETCH_ONE_WORD_SUCCESS = "FETCH_ONE_WORD_SUCCESS"
export const FETCH_ONE_WORD_FAILED = "FETCH_ONE_WORD_FAILED"

export const updateFilter = (filter, idModel, idCorpus) => dispatch => {
  dispatch({
    type: UPDATE_RESEARCH_FILTER,
    filter
  })

  if (filter !== "") {
    dispatch(fetchWords(idModel, idCorpus))
  }
}

export const fetchWords = (idModel, idCorpus) => (dispatch, getState) => {
  if (getResearchIsLoading(getState())) {
    return
  }

  dispatch({
    type: START_FETCHING_WORDS
  })

  const researchFilter = getResearchFilter(getState())
  return api
    .getWords(idModel, idCorpus, researchFilter, MAX_RESULT_PER_REQUEST)
    .then(
      words =>
        dispatch({
          type: FETCH_WORDS_SUCCESS,
          words
        }),
      error =>
        dispatch({
          type: FETCH_WORDS_FAILED,
          errorMessage: error.message
        })
    )
    .finally(() => {
      const filterAsChanged = researchFilter !== getResearchFilter(getState())
      if (filterAsChanged) {
        dispatch(fetchWords(idModel, idCorpus))
      }
    })
}

export const selectWord = (idModel, idCorpus, word) => (dispatch, getState) => {
  if (getSelectedWordIsLoading(getState())) {
    return
  }

  dispatch({ type: START_FETCHING_ONE_WORD })
  return api.fetchOneWord(idModel, idCorpus, word).then(
    ({ word, parameters }) => {
      dispatch({
        type: FETCH_ONE_WORD_SUCCESS,
        parameters,
        word
      })
      dispatch(push(`/modeles/${idModel}/corpus/${idCorpus}/word/${word}`))
    },
    error => {
      dispatch({
        type: FETCH_ONE_WORD_FAILED,
        errorMessage: error.message
      })
      toastr.error("error")
    }
  )
}
