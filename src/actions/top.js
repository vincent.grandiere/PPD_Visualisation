export const CHANGE_PARAMETERS = "CHANGE_PARAMETERS"

export const changeParameters = (n, asc, dimensions) => ({
  type: CHANGE_PARAMETERS,
  dimensions,
  asc,
  n,
})