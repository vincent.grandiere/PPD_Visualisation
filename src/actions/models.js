import api from "../api"

import { getModelListIsLoading, getModelToImportIsSending } from "../reducers"
import { push } from "react-router-redux"

export const START_FETCHING_MODELS = "START_FETCHING_MODELS"
export const FETCH_MODELS_SUCCESS = "FETCH_MODELS_SUCCESS"
export const FETCH_MODELS_FAILED = "FETCH_MODELS_FAILED"

export const START_IMPORTING_MODEL = "START_IMPORTING_MODEL"
export const IMPORT_MODEL_SUCCESS = "IMPORT_MODEL_SUCCESS"
export const IMPORT_MODEL_FAILED = "IMPORT_MODEL_FAILED"

export const fetchModels = () => (dispatch, getState) => {
  if (getModelListIsLoading(getState())) {
    return Promise.resolve()
  }

  dispatch({
    type: START_FETCHING_MODELS
  })
  return api.fetchModels().then(
    models => {
      dispatch({
        type: FETCH_MODELS_SUCCESS,
        models
      })
    },
    error => {
      dispatch({
        type: FETCH_MODELS_FAILED,
        errorMessage: error.message
      })
    }
  )
}

export const importModel = file => (dispatch, getState) => {
  if (getModelToImportIsSending(getState())) {
    return Promise.resolve()
  }

  dispatch({
    type: START_IMPORTING_MODEL
  })

  return api.importModel(file).then(
    result => {
      dispatch({
        type: IMPORT_MODEL_SUCCESS,
        model: result
      })
    },
    error => {
      dispatch({
        type: IMPORT_MODEL_FAILED,
        errorMessage: error.message
      })
    }
  )
}

export const selectModel = (event, row, rowIndex) => (dispatch, getState) =>
  dispatch(push(`/modeles/${row.id}`))
