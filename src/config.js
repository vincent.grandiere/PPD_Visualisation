import createHistory from 'history/createBrowserHistory'

export default {
  research: {
    MAX_RESULT_PER_REQUEST: 100
  },
  api: "localhost:3000",
  history: createHistory()
}
