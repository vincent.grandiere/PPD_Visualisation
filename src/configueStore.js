import { createStore, applyMiddleware } from "redux"
import config from "./config"

import { routerMiddleware } from "react-router-redux"
import promise from "redux-promise"
import thunk from "redux-thunk"
import logger from "redux-logger"

import appReducer from "./reducers"

const configureStore = () => {
  const middleWares = [promise, thunk, routerMiddleware(config.history)]

  if (process.env.NODE_ENV === "development") {
    middleWares.push(logger)
  }

  const store = createStore(
    appReducer,
    applyMiddleware(...middleWares)
  )

  return store
}

export default configureStore
