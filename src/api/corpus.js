export const importCorpus = fetch => file => {
  return fetch("corpus", {
    method: "post",
    body: { file }
  })
}

export const fetchCorpus = fetch => () => {
  return fetch("corpus", {
    method: "get"
  })
}
