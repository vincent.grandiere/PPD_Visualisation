import fakeFetch from "../fakeApi"
import config from "../config"

import * as modelsRequestsFactories from "./models"
import * as corpusRequestsFactories from "./corpus"
import * as vocabularyRequestsFactories from "./vocabulary"

const realFetch = (path, params) => fetch(`${config.api}/${path}`)

export const myFetch = (path, params) =>
  process.env.NODE_ENV === "development"
    ? fakeFetch(path, params)
    : realFetch(path, params)

const factories = {
  ...modelsRequestsFactories,
  ...vocabularyRequestsFactories,
  ...corpusRequestsFactories
}

const requestFactory = (factories, fetch) => {
  let requests = {}
  for (const i in factories) {
    requests[i] = factories[i](fetch)
  }
  return requests
}

export default requestFactory(factories, myFetch)
