export const getWords = fetch => (idModel, idCorpus, filter, maxResult) => {
  return fetch("vocabulary", {
    method: "get",
    body: {
      idModel,
      idCorpus,
      filter,
      maxResult
    }
  })
}

export const fetchOneWord = fetch => (idModel, idCorpus, word) => {
  return fetch("vocabulary/one", {
    method: "get",
    body: {
      idModel,
      idCorpus,
      word
    }
  })
}

export const fetchBoxPlot = fetch => (idModel, idCorpus) => {
  return fetch("vocabulary/boxplot", {
    method: "get",
    body: {
      idModel,
      idCorpus,
    }
  })
}
