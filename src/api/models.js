export const importModel = fetch => file => {
  return fetch("models", {
    method: "post",
    body: { file }
  })
}

export const fetchModels = fetch => () => {
  return fetch("models", {
    method: "get"
  })
}
