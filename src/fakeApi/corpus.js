const list = [{
  id: 1,
  name: 'un corpus'
},{
  id: 2,
  name: 'un autre corpus'
},{
  id: 3,
  name: 'Bonjour'
},{
  id: 4,
  name: 'La belle au bois dormant'
},{
  id: 5,
  name: 'un joli corpus'
}]

const corpus = {
  id: 6,
  name: 'un corpus ajouté'
}

export const importCorpus = (file) => {
  if (!file) {
    throw new Error("Fichier requis")
  }
  return corpus
}

export const fetchCorpus = () => {
  return list
}