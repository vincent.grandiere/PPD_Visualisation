import * as fromVocabulary from "./vocabulary"
import * as fromModels from "./models"
import * as fromCorpus from "./corpus"

const delay = ms => new Promise(resolve => setTimeout(resolve, ms))

const fakeFetch = (path, parameters) => {
  return delay(500).then(() => {
    if (Math.random() > 0.9) {
      throw new Error("Impossible de contacter le serveur")
    }

    switch (`${path}/${parameters.method}`) {
      case "vocabulary/get":
        return fromVocabulary.getWordsWithFilter(
          parameters.body.idModel,
          parameters.body.idCorpus,
          parameters.body.filter,
          parameters.body.maxResult
        )

      case "vocabulary/one/get":
        return fromVocabulary.getOneWord(
          parameters.body.idModel,
          parameters.body.idCorpus,
          parameters.body.word
        )

      case "vocabulary/boxplot/get":
        return fromVocabulary.getBoxPlot(
          parameters.body.idModel,
          parameters.body.idCorpus,
        )

      case "models/post":
        return fromModels.importModel(parameters.body.file)

      case "models/get":
        return fromModels.fetchModels()

      case "corpus/post":
        return fromCorpus.importCorpus(parameters.body.file)

      case "corpus/get":
        return fromCorpus.fetchCorpus()

      default:
        console.log(`${path}/${parameters.method}`)
    }
  })
}

export default fakeFetch
