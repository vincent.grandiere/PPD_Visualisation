const allWords = [
  "souffle",
  "toto",
  "titi",
  "mot",
  "tutu",
  "louis",
  "lars",
  "loulou",
  "le",
  "la",
  "bouffe",
  "souche"
]

const parameters = {
  frequency: 20,
  type: 'scientifique',
  before: "fizz",
  after: "buzz"
}

const boxPlot = {
  nbDimensions: 300,
  stats: [
    {
      word: "souffle",
      whiskerLow: 194.3,
      quartile1: 201,
      quartile2: 234.5,
      quartile3: 254.6,
      whiskerHigh: 257.95,
      outliers: [50, 75, 184.25, 268, 290]
    },
    {
      word: "toto",
      whiskerLow: 181.3,
      quartile1: 200,
      quartile2: 239.5,
      quartile3: 245.6,
      whiskerHigh: 272.95,
      outliers: [65, 170.25, 280, 295]
    },
    {
      word: "titi",
      whiskerLow: 197.3,
      quartile1: 204,
      quartile2: 230.5,
      quartile3: 259.6,
      whiskerHigh: 267.95,
      outliers: [129, 272, 279]
    },
    {
      word: "mot",
      whiskerLow: 184.3,
      quartile1: 202,
      quartile2: 234.5,
      quartile3: 266.6,
      whiskerHigh: 270.95,
      outliers: [156, 180.25, 290]
    },
    {
      word: "le",
      whiskerLow: 194.3,
      quartile1: 201,
      quartile2: 234.5,
      quartile3: 254.6,
      whiskerHigh: 257.95,
      outliers: [58, 72, 179]
    }
  ]
}

export const getWordsWithFilter = (
  idModel,
  idCorpus,
  filter = "",
  maxResult = 200
) => {
  if (!idModel) {
    throw new Error("merci de selectionner un modèle")
  }
  if (!idCorpus) {
    throw new Error("merci de selectionner un corpus")
  }

  return allWords
    .filter(word => word.startsWith(filter))
    .sort()
    .slice(0, maxResult)
}

export const getOneWord = (idModel, idCorpus, word) => {
  if (!idModel) {
    throw new Error("merci de selectionner un modèle")
  }
  if (!idCorpus) {
    throw new Error("merci de selectionner un corpus")
  }
  if (!word) {
    throw new Error("merci de selectionner un mot")
  }
  if (!allWords.some(w => w === word)) {
    throw new Error("merci de selectionner un mot présent dans le vocabulaire")
  }

  return {
    word,
    parameters
  }
}

export const getBoxPlot = (idModel, idCorpus) => {
  if (!idModel) {
    throw new Error("merci de selectionner un modèle")
  }
  if (!idCorpus) {
    throw new Error("merci de selectionner un corpus")
  }

  return boxPlot
}
