const models = [
  {
    id: 1,
    type: 'word2vec',
    architecture: "skip-gram",
    embedding_size: 1,
    window_size: 4,
    negative_samples: 6,
    min_count: 10
  },
  {
    id: 2,
    type: 'word3vec',
    architecture: "CBoW",
    embedding_size: 15,
    window_size: 14,
    negative_samples: 36,
    min_count: 106
  },
  {
    id: 3,
    type: 'word4vec',
    architecture: "CBoW",
    embedding_size: 21,
    window_size: 14,
    negative_samples: 56,
    min_count: 105
  }
]

const model = {
    id: 4,
    type: 'word5vec',
    architecture: "skip-gram",
    embedding_size: 11,
    window_size: 41,
    negative_samples: 16,
    min_count: 101
}


export const importModel = (file) => {
  if (!file) {
    throw new Error("Fichier requis")
  }
  return model
}

export const fetchModels = () => {
  return models
}