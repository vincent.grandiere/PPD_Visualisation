import {connect} from 'react-redux'
import View from './view'

const mapStateToProps = (state, router) => ({
  idModel: router.match.params.idModel,
  idCorpus: router.match.params.idCorpus,
})

const mapDispatchToProps = {}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(View)
