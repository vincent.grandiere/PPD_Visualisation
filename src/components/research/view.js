import React from 'react'
import { Link } from 'react-router-dom'

const Header = ({
  idModel, 
  idCorpus
}) => (
      <nav>
        <p class="choix">Choisissez votre type de recherche</p>
        <ul class="bouton">
          <li class="sousmenu1"><Link to={`/modeles/${idModel}/corpus/${idCorpus}/direct`}>Recherche directe</Link></li>
          <li class="sousmenu2"><Link to={`/modeles/${idModel}/corpus/${idCorpus}/visualisation`}>Visualisation de la matrice</Link></li>
          <li class="sousmenu3"><Link to={`/modeles/${idModel}/corpus/${idCorpus}/top`}>Top n</Link></li>
        </ul>
      </nav>
  )

export default Header