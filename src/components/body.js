import React from "react"
import { Route, Switch } from "react-router"

import ModelsContainer from "./models"
import CorpusContainer from "./corpus"
import WordResearchContainer from "./world-selection"
import GenericNotFound from "./generic/not-found"
import Research from "./research"
import Visualisation from "./box-plot"
import WordVisualisation from './word-visualisation'

const Body = () => (
  <main>
    <Switch>
      <Route exact path="/" component={ModelsContainer} />
      <Route exact path="/modeles/:idModel" component={CorpusContainer} />
      <Route
        exact
        path="/modeles/:idModel/corpus/:idCorpus"
        component={Research}
      />
      <Route
        exact
        path="/modeles/:idModel/corpus/:idCorpus/direct"
        component={WordResearchContainer}
      />
      <Route
        exact
        path="/modeles/:idModel/corpus/:idCorpus/visualisation"
        component={Visualisation}
      />
      <Route
        exact
        path="/modeles/:idModel/corpus/:idCorpus/word/:word"
        component={WordVisualisation}
      />
      <Route component={GenericNotFound} />
    </Switch>
  </main>
)

export default Body
