import React from "react"

import AutoComplete from "material-ui/AutoComplete"
import ErrorDisplay from "../generic/error-display"

import "./index.css"

const WordSearchView = ({
  filter,
  result,
  errorMessage,
  isLoading,
  fetchWords,
  updateFilter,
  selectWord,
  idModel,
  idCorpus,
}) => (
  <div>
    <AutoComplete
      hintText="Recherchez un mot"
      dataSource={result}
      onUpdateInput={(event) => updateFilter(event, idModel, idCorpus)}
      className={isLoading ? "loadinggif" : ""}
    />
    <br/>
    <button onClick={() => selectWord(idModel, idCorpus, filter)}>Valider</button>
    <ErrorDisplay onRetry={() => fetchWords(idModel, idCorpus)} hidden={errorMessage === null}>
      {errorMessage}
    </ErrorDisplay>
  </div>
)

export default WordSearchView
