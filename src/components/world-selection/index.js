import { connect } from "react-redux"

import WordResearchView from "./view"

import { fetchWords, updateFilter, selectWord } from "../../actions/research"
import { getResearchFilter, getResearchResult, getResearchErrorMessage, getResearchIsLoading } from '../../reducers'

const mapStateToProps = (state, router) => ({
  idModel: router.match.params.idModel,
  idCorpus: router.match.params.idCorpus,
  filter: getResearchFilter(state),
  result: getResearchResult(state),
  errorMessage: getResearchErrorMessage(state),
  isLoading: getResearchIsLoading(state),
})

const mapDispatchToProps = {
  fetchWords,
  updateFilter,
  selectWord
}

export default connect(mapStateToProps, mapDispatchToProps)(WordResearchView)
