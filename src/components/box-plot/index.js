import React from "react"
import View from "./view"

import { getBoxPlot, deleteWord, selectWord } from "../../actions"
import { connect } from "react-redux"
import {
  getBoxPlotList,
  getBoxPlotIsLoading,
  getBoxPlotErrorMessage,
  getBoxPlotNbDimentions
} from "../../reducers"

class BoxPlot extends React.Component {
  componentDidMount() {
    this.props.getBoxPlot(this.props.idModel, this.props.idCorpus)
  }

  render() {
    return <View {...this.props} />
  }
}

const mapStateToProps = (state, router) => ({
  idModel: router.match.params.idModel,
  idCorpus: router.match.params.idCorpus,
  nbDimentions: getBoxPlotNbDimentions(state),
  list: getBoxPlotList(state),
  errorMessage: getBoxPlotErrorMessage(state),
  isLoading: getBoxPlotIsLoading(state)
})

const mapDispatchToProps = {
  getBoxPlot,
  deleteWord,
  selectWord
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BoxPlot)
