import React from "react"
import { Boxplot, computeBoxplotStats } from "react-boxplot"
import ErrorDisplay from "../generic/error-display"
import Loader from "../generic/loader"

import "./index.css"

const BoxPlotView = ({
  list,
  errorMessage,
  isLoading,
  idModel,
  idCorpus,
  getBoxPlot,
  deleteWord,
  selectWord,
  nbDimentions
}) => (
  <div class="decalage">
    <Loader isDisplayed={isLoading && list.length === 0 ? true : false} />
    <table className="box-plot-table">
      <tbody>
        {list.map(({ word, ...stats }) => (
          <tr key={word}>
            <td>
              <span onClick={() => selectWord(idModel, idCorpus, word)}>{word}</span> 
              <a onClick={() => deleteWord(word)}> [x] </a>{" "}
            </td>
            <td>
              <Boxplot
                width={400}
                height={25}
                orientation="horizontal"
                min={0}
                max={nbDimentions}
                stats={stats}
              />
            </td>
          </tr>
        ))}
      </tbody>
    </table>
    <ErrorDisplay
      hidden={errorMessage === "" ? true : false}
      onRetry={() => getBoxPlot(idModel, idCorpus)}
    >
      {errorMessage}
    </ErrorDisplay>
  </div>
)

// class BoxPlotView extends Component {
//   render() {
//     return (
//       <div>

//       <Boxplot
//         width={400}
//         height={25}
//         orientation="horizontal"
//         min={0}
//         max={300}
//         stats={{
//           whiskerLow: 194.3,
//           quartile1: 201,
//           quartile2: 234.5,
//           quartile3: 254.6,
//           whiskerHigh: 257.95,
//           outliers: [50, 75, 184.25, 268, 290]
//         }}
//       />

//       <br/>

//       <Boxplot
//         width={400}
//         height={25}
//         orientation="horizontal"
//         min={0}
//         max={300}
//         stats={{
//           whiskerLow: 194.3,
//           quartile1: 201,
//           quartile2: 234.5,
//           quartile3: 254.6,
//           whiskerHigh: 257.95,
//           outliers: [50, 75, 184.25, 268, 290]
//         }}
//       />
//       </div>
//     )
//   }
// }

export default BoxPlotView
