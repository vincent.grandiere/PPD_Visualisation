import View from "./view"
import { connect } from "react-redux"

import { getSelectedWordParameters } from "../../reducers"

const mapStateToProps = (state, router) => ({
  idModel: router.match.params.idModel,
  idCorpus: router.match.params.idCorpus,
  word: router.match.params.word,
  parameters: getSelectedWordParameters(state)
})

const mapDispatchToProps = {}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(View)
