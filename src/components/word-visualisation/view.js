import React from "react"

const WordVisualisationView = ({ word, parameters }) => (
  <div>
    <ul>
      <li>Mot: {word}</li>
      <li>Fréquence: {parameters.frequency}</li>
      <li>Type: {parameters.type}</li>
      <li>Fréquence élevé en amont: {parameters.before}</li>
      <li>Fréquence élevé en aval: {parameters.after}</li>
    </ul>
  </div>
)

export default WordVisualisationView
