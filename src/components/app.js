import React from "react"

import Header from './header'
import Body from "./body"
import Overlay from './overlay'

const App = () => (
  <div>
    <Header />
    <Overlay />
    <Body />
  </div>
)

export default App
