import React from "react"

import config from '../config'
import { ConnectedRouter } from 'react-router-redux'
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider"
import { Provider } from "react-redux"

import App from "./app"

const Root = ({ store }) => (
  <Provider store={store}>
    <MuiThemeProvider>
      <ConnectedRouter history={config.history}>
        <App />
      </ConnectedRouter>
    </MuiThemeProvider>
  </Provider>
)

export default Root
