import React from "react"

import ErrorDisplay from "../../generic/error-display"

const ModelsErrorDisplay = ({ fetchModels, errorMessage }) => (
  <ErrorDisplay onRetry={fetchModels} hidden={errorMessage === null}>
    {errorMessage}
  </ErrorDisplay>
)

export default ModelsErrorDisplay
