import React from "react"
import { connect } from "react-redux"

import { fetchModels, selectModel } from "../../../actions/models"
import {
  getModelListElements,
  getModelListIsLoading,
  getModelListErrorMessage,
  getModelListIsInit
} from "../../../reducers"

import ModelsView from "./view"

class ModelsContainer extends React.Component {
  componentDidMount() {
    if (!this.props.isInit) {
      this.props.fetchModels()
    }
  }

  render() {
    return <ModelsView {...this.props} />
  }
}

const mapStateToProps = state => ({
  models: getModelListElements(state),
  isLoading: getModelListIsLoading(state),
  errorMessage: getModelListErrorMessage(state),
  isInit: getModelListIsInit(state)
})

const mapDispatchToProps = {
  fetchModels,
  selectModel
}

export default connect(mapStateToProps, mapDispatchToProps)(ModelsContainer)
