import React from "react"

import BootstrapTable from "react-bootstrap-table-next"

const columns = [
  {
    dataField: "type",
    text: "Type"
  },
  {
    dataField: "architecture",
    text: "Architecture"
  },
  {
    dataField: "embedding_size",
    text: "Embedding Size"
  },
  {
    dataField: "window_size",
    text: "Window Size"
  },
  {
    dataField: "negative_samples",
    text: "Negative Samples"
  },
  {
    dataField: "min_count",
    text: "Min Count"
  }
]

const ModelsTable = ({ models, isLoading, selectModel }) => (
  <BootstrapTable
    keyField="id"
    rowEvents={{ onClick: selectModel }}
    data={models}
    columns={columns}
  />
)

export default ModelsTable
