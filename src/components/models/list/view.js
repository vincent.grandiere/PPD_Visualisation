import React from "react"

import ModelsTable from "./table"
import ModelsErrorDisplay from "./error-display"

import "./index.css"

const ModelsView = props => (
  <div>
    <ModelsTable {...props} />
    <ModelsErrorDisplay {...props} />
  </div>
)

export default ModelsView
