import React from "react"

const ImportButton = ({ onChange }) => (
  <input
    value=""
    onChange={onChange}
    type="file"
    name="inputfile"
    className="inputfile"
  />
)

export default ImportButton
