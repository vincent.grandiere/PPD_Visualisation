import { connect } from "react-redux"

import { importModel } from "../../../actions/models"
import {
  getModelToImportIsSending,
  getModelToImportErrorMessage,
} from "../../../reducers"

import ModelsToImportView from "./view"


const mapStateToProps = state => ({
  isSending: getModelToImportIsSending(state),
  errorMessage: getModelToImportErrorMessage(state),
})

const mapDispatchToProps = {
  importModel
}

export default connect(mapStateToProps, mapDispatchToProps)(ModelsToImportView)
