import React from "react"

import ImportButton from './import-button'
import Loader from "../../generic/loader"
import ErrorDisplay from "../../generic/error-display"

const ImportModelView = ({ isSending, errorMessage, importModel }) => (
  <div>
    <ImportButton onChange={importModel} />
    <Loader isDisplayed={isSending} />
    <ErrorDisplay>{errorMessage}</ErrorDisplay>
  </div>
)

export default ImportModelView
