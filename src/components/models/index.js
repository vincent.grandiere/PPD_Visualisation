import React from 'react'

import ModelImport from './import'
import ModelList from './list'

const modelsPage = () => (
  <section>
    <ModelImport/>
    <ModelList/>
  </section>
)

export default modelsPage