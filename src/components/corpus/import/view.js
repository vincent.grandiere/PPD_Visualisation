import React from "react"

import ImportButton from './import-button'
import Loader from "../../generic/loader"
import ErrorDisplay from "../../generic/error-display"

const ImportCorpusView = ({ isSending, errorMessage, importCorpus }) => (
  <div>
    <ImportButton onChange={importCorpus} />
    <Loader isDisplayed={isSending} />
    <ErrorDisplay>{errorMessage}</ErrorDisplay>
  </div>
)

export default ImportCorpusView
