import { connect } from "react-redux"

import { importCorpus } from "../../../actions/corpus"
import {
  getCorpusToImportIsSending,
  getCorpusToImportErrorMessage,
} from "../../../reducers"

import CorpusToImportView from "./view"


const mapStateToProps = state => ({
  isSending: getCorpusToImportIsSending(state),
  errorMessage: getCorpusToImportErrorMessage(state),
})

const mapDispatchToProps = {
  importCorpus
}

export default connect(mapStateToProps, mapDispatchToProps)(CorpusToImportView)
