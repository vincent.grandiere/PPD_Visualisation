import React from 'react'

import CorpusImport from './import'
import CorpusList from './list'

const corpusPage = () => (
  <section>
    <CorpusImport/>
    <CorpusList />
  </section>
)

export default corpusPage