import React from "react"

import ErrorDisplay from "../../generic/error-display"

const CorpusErrorDisplay = ({ fetchCorpus, errorMessage }) => (
  <ErrorDisplay onRetry={fetchCorpus} hidden={errorMessage === null}>
    {errorMessage}
  </ErrorDisplay>
)

export default CorpusErrorDisplay
