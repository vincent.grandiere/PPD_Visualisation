import React from "react"

import ErrorDisplay from "./error-display"

const CorpusView = ({
  corpusList,
  isLoading,
  errorMessage,
  isInit,
  selectCorpus,
  fetchCorpus
}) =>
  isLoading ? (
    <p>...chargement</p>
  ) : (
    <div>
      <ul>
        {corpusList.map(corpus => (
          <li onClick={() => selectCorpus(corpus.id)} key={corpus.id}>
            {corpus.name}
          </li>
        ))}
      </ul>
      <ErrorDisplay fetchCorpus={fetchCorpus} errorMessage={errorMessage} />
    </div>
  )

export default CorpusView
