import React from "react"
import { connect } from "react-redux"

import { fetchCorpus, selectCorpus } from "../../../actions/corpus"
import {
  getCorpusListElements,
  getCorpusListIsLoading,
  getCorpusListErrorMessage,
  getCorpusListIsInit
} from "../../../reducers"

import CorpusView from "./view"

class CorpusContainer extends React.Component {
  componentDidMount() {
    if (!this.props.isInit) {
      this.props.fetchCorpus()
    }
  }

  render() {
    return <CorpusView {...this.props} />
  }
}

const mapStateToProps = state => ({
  corpusList: getCorpusListElements(state),
  isLoading: getCorpusListIsLoading(state),
  errorMessage: getCorpusListErrorMessage(state),
  isInit: getCorpusListIsInit(state)
})

const mapDispatchToProps = {
  fetchCorpus,
  selectCorpus
}

export default connect(mapStateToProps, mapDispatchToProps)(CorpusContainer)
