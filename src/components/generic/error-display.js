import React from "react"

const ErrorDisplay = ({ onRetry, children, hidden = false }) => (
  <div style={hidden ? { display: "none" } : {}}>
    <p> {children} </p>
    <button onClick={onRetry} className={onRetry ? "" : "hidden"}>Réessayer</button>
  </div>
)

export default ErrorDisplay
