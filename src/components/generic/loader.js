import React from "react"

const Loader = ({ isDisplayed }) => (
  <span className={isDisplayed ? "" : "hidden"}>Chargement...</span>
)

export default Loader
