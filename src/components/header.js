import React from "react"
import config from "../config"
import { Link } from "react-router-dom"

const Header = () => (
  <header className="HeadMenu">
    <center>
      <Link to="/">
      <img
        className="imgdescartes"
        src="https://image.noelshack.com/fichiers/2018/24/4/1528969398-petit-logo.png"
      />
      </Link>
    </center>
    {/* <nav>
        <ul id="navigation">
          <li><Link to='/'>Home</Link></li>
          <li><Link to='/modeles'>Modèles</Link></li>
          <li><Link to='/corpus'>Corpus</Link></li>
          <li><a href="#">Selection des mots</a>
            <ul className="submenu">
              <li><Link to='/recherchemot'>Recherche directe du mot</Link></li>
              <li><Link to='/visudim'>Visualisation des dimentions</Link></li>
              <li><Link to='/topn'>Top n</Link></li>
            </ul>
          </li>
        </ul>
      </nav> */}
  </header>
)

export default Header
