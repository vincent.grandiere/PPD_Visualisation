export const pushNewValue = (array, value) =>
  array.includes(value) ? array : [...array, value]

export const mergeArrayUnique = (array1, array2) =>
  array2.reduce(pushNewValue, array1)
