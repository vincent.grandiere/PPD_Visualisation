import { pushNewValue, mergeArrayUnique } from "./array-tools"

describe("mergeArrayUnique", () => {
  it("Merge well", () => {
    const givenArray = ["tomato", "ketchup"]
    const givenValue = ["pasta", "yogurt"]
    const expectedArray = ["tomato", "ketchup", "pasta", "yogurt"]

    expect(mergeArrayUnique(givenArray, givenValue)).toEqual(expectedArray)
  })

  it("Merge only unique", () => {
    const givenArray = ["tomato", "ketchup"]
    const givenValue = ["tomato", "yogurt"]
    const expectedArray = ["tomato", "ketchup", "yogurt"]

    expect(mergeArrayUnique(givenArray, givenValue)).toEqual(expectedArray)
  })
})

describe("pushNewValue", () => {
  it("Push value if it's new", () => {
    const givenArray = ["tomato", "ketchup"]
    const givenValue = "pasta"
    const expectedArray = ["tomato", "ketchup", "pasta"]

    expect(pushNewValue(givenArray, givenValue)).toEqual(expectedArray)
  })

  it("Don't push value if it's not new", () => {
    const givenArray = ["tomato", "ketchup"]
    const givenValue = "tomato"
    const expectedArray = ["tomato", "ketchup"]

    expect(pushNewValue(givenArray, givenValue)).toEqual(expectedArray)
  })
})
